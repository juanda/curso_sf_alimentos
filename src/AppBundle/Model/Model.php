<?php

namespace AppBundle\Model;

class Model {

    protected $conexion;

    public function __construct($dbname, $dbuser, $dbpass, $dbhost) {
        $dsn = "mysql:host=$dbhost;dbname=$dbname;charset=utf8mb4";

        $this->pdo = new \PDO($dsn, $dbuser, $dbpass);
    }

    public function dameAlimentos() {
        $sql = "select * from alimentos order by energia desc";

        $stmt = $this->pdo->query($sql);

        $alimentos = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $alimentos;
    }

    public function buscarAlimentosPorNombre($nombre) {
        $nombre = \htmlspecialchars($nombre);

        $sql = "select * from alimentos where nombre like :nombre order by energia desc";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(array(':nombre' => $nombre));

        $alimentos = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $alimentos;
    }

    public function dameAlimento($id) {
        $id = \htmlspecialchars($id);

        $sql = "select * from alimentos where id=:id";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(array(':id' => $id));

        $alimentos = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (count($alimentos) == 0) {
            return FALSE;
        }

        return $alimentos[0];
    }

    public function insertarAlimento($n, $e, $p, $hc, $f, $g) {
        $n = \htmlspecialchars($n);
        $e = \htmlspecialchars($e);
        $p = \htmlspecialchars($p);
        $hc = \htmlspecialchars($hc);
        $f = \htmlspecialchars($f);
        $g = \htmlspecialchars($g);

        $sql = "insert into alimentos (nombre, energia, proteina, hidratocarbono,
        fibra, grasatotal) values (:nombre, :energia, :proteina, :hidratocarbono,
        :fibra, :grasatotal)";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(array(
            ':nombre' => $n,
            ':energia' => $e,
            ':proteina' => $p,
            ':hidratocarbono' => $hc,
            ':fibra' => $f,
            ':grasatotal' => $g
        ));

        return $stmt->rowCount();
    }

}
