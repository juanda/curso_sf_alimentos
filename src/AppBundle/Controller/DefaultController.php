<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Model\Model;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        $params = array(
            'mensaje' => 'Bienvenido al curso de Symfony2',
            'fecha' => date('d-m-yy'),
        );

        return $this->render('default/index.html.twig', $params);
    }

    private function getModel() {
        $m = $this->get('app.model');

        return $m;
    }

    /**
     * @Route("/listar", name="listar")
     */
    public function listarAction() {

        $m = $this->getModel();

        $params = array(
            'alimentos' => $m->dameAlimentos(),
        );

        return $this->render('default/mostrar_alimentos.html.twig', $params);
    }

    /**
     * @Route("/insertar", name="insertar")
     */
    public function insertarAction(Request $request) {
        $params = array(
            'nombre' => '',
            'energia' => '',
            'proteina' => '',
            'hc' => '',
            'fibra' => '',
            'grasa' => '',
        );

        $m = $this->getModel();

        if ($request->getMethod() == 'POST') {

            // comprobar campos formulario
            if ($m->insertarAlimento(
                            $request->get('nombre'), $request->get('energia'),
                            $request->get('proteina'), $request->get('hc'), 
                            $request->get('fibra'), $request->get('grasa'))) {
                $params['mensaje'] = 'Alimento insertado correctamente';
            } else {
                $params = array(
                    'nombre' => $request->get('nombre'),
                    'energia' => $request->get('energia'),
                    'proteina' => $request->get('proteina'),
                    'hc' => $request->get('hc'),
                    'fibra' => $request->get('fibra'),
                    'grasa' => $request->get('grasa'),
                );
                $params['mensaje'] = 'No se ha podido insertar el alimento.
                                      Revisa el formulario';
            }
        }
        return $this->render('default/form_insertar.html.twig', $params);
    }

    /**
     * @Route("/buscar", name="buscar")
     */
    public function buscarPorNombreAction(Request $request) {
        $params = array(
            'nombre' => '',
            'resultado' => array(),
        );

        $m = $this->getModel();

        if ($request->getMethod() == 'POST') {
            $params['nombre'] = $request->get('nombre');
            $params['resultado'] = $m->buscarAlimentosPorNombre($request->get('nombre'));
        }
        return $this->render('default/buscar_por_nombre.html.twig', $params);
    }

    /**
     * @Route("/ver/{id}", name="ver")
     */
    public function verAction($id, Request $request) {

        $m = $this->getModel();

        $alimento = $m->dameAlimento($id);

        if (!$alimento) {
            throw $this->createNotFoundException('No se encuentra ese alimento');
        }

        $params = $alimento;

        return $this->render('default/ver_alimento.html.twig', $params
        );
    }

}
